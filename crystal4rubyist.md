---
author: David YOTEAU
date: DD-MM-YYYY
paging: Crystal pour les Rubyist. pages %d sur %d
marp: true
paginate: true
---

# Crystal pour les Rubyist

---

## Pourquoi Crystal ?

* Fusion de l'élégance de Ruby avec les performances de C
* Caractéristiques uniques : types statiques, macros puissantes
* Les frameworks web similaires à Rails et Sinatra

---

## Pourquoi ajouter Crystal à votre boite à outils ?

* ✔️ Crystal a une superbe syntaxe comme Ruby
* ✔️ Les performances de Crystal sont comparables à celles de C
* ✔️ Crystal prend en charge les liaisons natives en C
* ✔️ Types statiques et puissants macros pour la métaprogrammation
* ✔️ Crystal dispose de frameworks web similaires à Rails et Sinatra

---

## Syntaxe quasi identique mais ...

---

## Syntaxe quasi identique mais ...
### Fortement typé

* Crystal impose des types statiques pour garantir la sûreté du code
* Les erreurs de typage sont détectées à la compilation, évitant les bugs en production
* La vérification des types permet une documentation précise et une meilleure compréhension du code

---
## Syntaxe quasi identique mais ...
### Fortement typé

Exemple

```crystal
def add(a : Int, b : Int) : Int
  return a + b
end

puts add("5", 3)
```

```shell
Error: expected argument #1 to 'add' to be Int, not String

Overloads are:
 - add(a : Int, b : Int)
```

---

## Syntaxe quasi identique mais ...
### Les entiers.

En Ruby, vous avez le type Fixnum. En Crystal, vous allez devoir définir la taille : Int8, Int16, Int32, Int64, UInt8, UInt16, UInt32, or UInt64

```crystal
x = 127_i8 # An Int8 type
x          # => 127
x += 1     # Unhandled exception: Arithmetic overflow (OverflowError)
```

---

## Syntaxe quasi identique mais ...
### La boucle `.each` peut être surprenant

*Ruby*
```ruby 
[1, 2].each { "foo" } # => [1, 2]
```

*Crystal*
```crystal 
[1, 2].each { "foo" }       # => nil
[1, 2].tap &.each { "foo" } # => [1, 2]
```

---

## Syntaxe quasi identique mais ...
### Définition des attributs d'une classe

En Crystal

```crystal
class Meetup
  property  title : String
  getter    date  : Time
  property? open  : Bool = true

  def initialize(@title, @date)
  end
end
```

---

## Syntaxe quasi identique mais ...
### Définition des attributs d'une classe

| Ruby Keyword | Crystal |
| ------------ | ------- |
| attr_accessor | property |
| attr_reader | getter |
| attr_writer | setter |

---

## Parlons performances.
### Ruby vs Crystal vs C

* source : programming-language-benchmarks.vercel.app
* date : 1 février 2024

Algorythme nbody sur 500 000 d'entrées

| Lang | time | stddev | peak-mem | time(user) | time(sys) |
| ---- | ---- | ------ | -------- | ---------- | --------- |
| ruby (3.3.0) | 2 876 ms | 37 ms | 28.1 Mb | 2 840 ms | 20 ms |
| crystal (1.11.2) | 37 ms | 0.5 ms | 3.5 Mb | 27 ms | 0 ms |
| c (gcc 13.2.0) | 28 ms | 0.5 ms | 2.3 Mb | 20 ms | 0 ms |


---

## Parlons performances.
### Et coté performances web.

* source : https://web-frameworks-benchmark.netlify.app/result
* date : 18 mars 2024

| Framework | Req/sec | P90 | P99.999 | Latence moyenne |
| --------- | ------- | --- | ------- | --------------- |
| Ruby Sinatra | 11 144 | 220.79 | 312.56 | 71.69 |
| Ruby Rails | 4 928 | 452.04 | 619.54 | 146.62 |
| Ruby Hanami | 17 694 | 175.62 | 257.07 | 55.74 |
| C Agoo | 155 260 | 3.90 | 16.74 | 1.84 |
| Crystal Kemal | 170 949 | 3.34 | 19.72 | 1.51 |
| Crystal Amber | 158 749 | 3.95 | 22.15 | 1.73 |
| Crystal Spider-Gazelle | 177 954 | 3.27 | 24.01 | 1.47 |

---

## Une petite demo?

---

## Ils utilisent Crystal en production.

* Manas Tech : Société mère de Crystal-Lang
* 84codes : Provider SaaS et Editeur de LavinMQ
* JoystickTV : Plateforme de Stream
* PlaceOS : Gestion de batiment et mainteneur de Spider-Gazelle
* Nikola Corp : Constructeur automobile.
* RainforestQA : Produit Saas autour de la QA
* Bright Security : Cyber Security.

---

## Ou trouver la suite?

* le site de crystal : https://crystal-lang.org
* le discord : https://discord.gg/YS7YvQy
* le site crystal for rubyist : https://www.crystalforrubyists.com

---

## Qui suis-je?

### David YOTEAU

* Ingénieur logiciel
* Ambassadeur Crystal
* Papa poule
* Cuisinier amateur

Merci de m'avoir écouté

---

## Questions?
